<?php
//Damien Degrelle
class Fichier{
	private $_nom; // Contient le nom du fichier
	private $_arbo;// Contient l'arborescence du fichier depuis le dossier de l'utilisateur
	private $_data;// Contient les données du fichier
	private $_user;// Contient l'id de l'utilisateur

	//Constructeur de Fichier
	public function __construct($nom,$arbo,$user)
	{
  	$this->setNom($nom); // Insère le nom du fichier
  	$this->setArbo($arbo);//Insère l'arborescence du fichier
  	$this->setUser($user);//Insère l'id de l'utilisateur
		$file = fopen('/var/www/phpProjet/Model/Users/'.$user.$arbo.$nom.'.java', 'r+'); //Ouvre le fichier
		$this->setData(fread($file, filesize ('/var/www/phpProjet/Model/Users/'.$user.$arbo.$nom.'.java'))); //Insère les données du fichier en lisant le fichier
		fclose($file);//Ferme le fichier
	}

	public function setNom($nom)
	{
	   $this->_nom=$nom;//Actualise le nom du fichier
	}

	public function getNom()
	{
	   return $this->_nom;//Renvoie le nom du fichier
	}

	public function setArbo($arbo)
	{
	   $this->_arbo=$arbo;//Actualise l'arborescence
	}

	public function getArbo()
	{
  	return $this->_arbo;//Renvoie l'arborescence
	}

	public function setUser($user)
	{
  	$this->_user=$user;//Actualise l'id
	}

	public function getUser()
	{
  	return $this->_user;//Renvoie l'id
	}

	public function setData($data)
	{
  	$this->_data=$data;//Actualise l'id
	}

	public function getData()
	{
  	return $this->_data;//Renvoie l'id
	}

	//Fonction d'écriture du fichier
	public function write(){
		$file = fopen('/var/www/phpProjet/Model/Users/'.$this->_user.$this->_arbo.$this->_nom.'.java', 'r+');//Ouvre le fichier
		fseek($file, 0);//Met le pointeur au debut du fichier
		fputs($file, $this->_data);//Ecris les données
		fclose($file);//Ferme le fichier
	}


}
?>
