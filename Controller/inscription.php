<?php
//Cécil Thomas
function comparePassword($pass,$pass2){
	$error = false;
	if($pass !== $pass2)
	{
		$error = true;
	}
  return $error;
}

function compareMail($mail,$mail2){
	$error = false;
	if($mail !== $mail2)
	{
		$error = true;
	}
    return $error;
}

function verifMail($mail){
  require('../Controller/bdd.php');
	$error=false;
	$query = $bdd->prepare("SELECT * FROM membre WHERE mail = :mail");
	$query->execute(array('mail' => $mail));
	$count = $query->rowCount();
	if($count>0)
	{
		$error=true;
	}


	return $error;
}

function verifPseudo($pseudo){
  require('../Controller/bdd.php');
	$error=false;
	$query = $bdd->prepare("SELECT * FROM membre WHERE login = :pseudo");
	$query->execute(array('pseudo' => $pseudo));
	$count = $query->rowCount();
	if($count>0)
	{
		$error=true;
	}

	return $error;
}

function insertTable($pseudo,$mail,$pass)
{
  require('../Controller/bdd.php');
	$error = false;
  $insertmbr = $bdd->prepare("INSERT INTO membre(login, mail, password) VALUES(?,?,?)");
  $insertmbr-> execute(array($pseudo,$mail,sha1($pass)));
	$query = $bdd->prepare("SELECT id FROM utilisateurs WHERE login = :pseudo, mail=:mail");
	$query->execute(array('pseudo' => $pseudo,'mail' => $mail));
	$count = $query->rowCount();
	if($count!==0){
		$error = true;
	}
    return $error;
}

 ?>
