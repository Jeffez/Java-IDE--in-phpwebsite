<?php
//Cécil Thomas
//Verifie si l'utilisateur peut se connecter
function verifConnexion($password,$mail,$pseudo){
  require('../Controller/bdd.php'); //Recupère le fichier bdd.php
	$error = false;//Initialise la variable error a false
  global $idUser;// Creer la variable idUser de façon globale (Accessible depuis n'importe quel endroit du code)
  if($error===false){//Test si la variable error est a faux
	  $reqpassword= $bdd->prepare("SELECT * FROM membre ");//Prepare la requete SQL de selection de tous les membres
	  $reqpassword->execute();//Execute la requete
    $error=true;//Attribue la valeur true a error
    while($rowMail = $reqpassword->fetch()){//Pour tous les membres
      if(($rowMail['mail']===$mail||$rowMail['login']===$pseudo)&&$rowMail['password']===$password){//Test si le mail de params== mail de la base ou si le pseudo de params==pseudo de la base, et si le mdp de params == mdp de la base
        $error=false;//Attribue la valeur false a error
        $idUser=$rowMail['id'];//Attribue la valeur de l'id du membre a idUser
      }
    }
  }
	return $error;//Retourne la variale
}
//Efface tous les fichiers erreur et resultats
function dropFile($id){
  $file = fopen('../Model/Users/'.$id.'/compil.error',"w");//Ouvre le fichier compil.error
  ftruncate($file,0);//Efface le fichier compil.error
  fclose($file);//Ferme le fichier compil.error
  $file = fopen('../Model/Users/'.$id.'/exec.error',"w");//Ouvre le fichier exec.error
  ftruncate($file,0);//Efface le fichier exec.error
  fclose($file);//Ferme le fichier exec.error
  $file = fopen('../Model/Users/'.$id.'/compil.result',"w");//Ouvre le fichier compil.result
  ftruncate($file,0);//Efface le fichier compil.result
  fclose($file);//Ferme le fichier compil.result
  $file = fopen('../Model/Users/'.$id.'/exec.result',"w");//Ouvre le fichier exec.result
  ftruncate($file,0);//Efface le fichier exec.result
  fclose($file);//Ferme le fichier exec.result
}
