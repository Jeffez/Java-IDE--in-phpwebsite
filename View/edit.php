<?php
//Damien Degrelle
require('../Controller/edit.php');  //Recupère le fichier edit.php
require('../Controller/compil.php'); //Recupère le fichier compil.php
require('../Controller/exec.php'); //Recupère le fichier exec.php
require('../Controller/connexion.php');//Recupère le fichier connexion.php
$id;//Intialise la variable id
if(isset($_SESSION['id'])){//Test si il y a une valeur dans la session id
  $id = $_SESSION['id']; // Si oui, attribue cette valeur a id
}else{
  $id=-1; // Si non, attribue la valeur -1 a id
}
?>
<head>
  <link rel="stylesheet" href="src/style.css">
  <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
  <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <script src = "src/jQuery.js"></script>
</head>
<div id="tree">
  <ul>
    <?php require('tree.php');//Recupère le fichier tree.php?>
  </ul>
</div>
<div id="content">
  <?php
  if(isset($_GET['name'])&&isset($_GET['arbo'])){//Test si il y a des valeurs dans le get name et le get arbo
    $file = new Fichier($_GET['name'],$_GET['arbo'],$_SESSION['id']); //Si oui, creer un nouveau fichier avec l'id, le nom, et l'arbo
  }else if(isset($_POST['name'])&&isset($_POST['arbo'])&&isset($_POST['id'])){// si non, test si il y a des valeurs dans le post name, le post arbo et le post id
    $file = new Fichier($_POST['name'],$_POST['arbo'],$_POST['id']);//Si oui, creer un nouveau fichier avec l'id, le nom, et l'arbo
    $file->setData($_POST['data']);//Actualise les donnees
    $file->write();//Ecris les donnees dans le fichier
    dropFile($_POST['id']);
    compil($file);// Compile le fichier
    execSh($file);// Execute le fichier
  }
 ?>
  <form action="edit.php" method="post">
    <input type="hidden" name="name" value="<?php if(isset($_GET['name'])){echo $_GET['name'];}else if(isset($_POST['name'])){echo $_POST['name'];}//Affiche le get name si il est present, ou le post name si il est present?>">
    <input type="hidden" name="arbo" value="<?php if(isset($_GET['arbo'])){echo $_GET['arbo'];}else if(isset($_POST['arbo'])){echo $_POST['arbo'];}//Affiche le get arbo si il est present, ou le post arbo si il est present?>">
    <input type="hidden" name="id" value="<?php if(isset($_SESSION['id'])){echo $_SESSION['id'];}else if(isset($_POST['id'])){echo $_POST['id'];}//Affiche le get id si il est present, ou le post id si il est present?>">
    <textarea name="data"><?php if(isset($file)){echo $file->getData();}//Affiche les donnees du fichier?></textarea><br>
    <div id="tabs">
    <ul>
		<li><a href="#content-1">Output Compilation</a></li>
		<li><a href="#content-2">Output Result</a></li>
		<input type="submit" value="Sauvegarder, Compiler et Executer"></br></br>
    </ul>
	</div>
  </form>
  <?php require('consol.php');//Recupère le fichier consol.php?>
</div>
