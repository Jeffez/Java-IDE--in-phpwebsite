<?php
//Damien Degrelle
$id= $_SESSION['id']; //Recupère l'id via la variable de session id
 ?>
<div id="console">
	<div id="content-1">
	<?php
		$file = '../Model/Users/'.$id.'/compil.error'; //Attribue a file le chemin de compil.error
		if(filesize($file)==0){//Test si le fichier compil.error est vide
		echo '<p class="result">Compilation ok !</p>';//Si oui, affiche "Compilation ok"
		}else{
		echo '<p class="error">';//Si non, affiche l'erreur contenu dans le fichier
		readfile($file);
		echo '</p>';
	}
	?>
	</div>
	<div id="content-2">
		<p class="error">
			<?php
				$file = '../Model/Users/'.$id.'/exec.error';//Attribue a file le chemin de exec.error
				readfile($file);//Affiche le contenu du fichier
			?>
		</p>
		<p class="result">
			<?php
				$file = '../Model/Users/'.$id.'/exec.result';//Attribue a file le chemin de exec.result
				readfile($file);//Affiche le contenu du fichier
			?>
		</p>
	</div>

</div>
